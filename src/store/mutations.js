export default {
    flipDrawerState(state) {
        state.drawer = !state.drawer
    }
}