export default {
    drawer: false,
    navDrawerItems: [
        { title: 'Dashboard', icon: 'mdi-view-dashboard', route: '/' },
        { title: 'Impostazioni', icon: 'mdi-hammer-screwdriver', route: '/tools' }
    ]
}